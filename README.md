# UK Biobank Processing
## Introduction

This is a software that automatically transform the tab separated UK Biobank phenotype file into a sqlite3 database. 
Each phenotype will be stored in a separated file called `f<fieldID>`, which include the following fields

1. Sample ID
2. Instance
3. Phenotype
4. Array (if this phenotype is an arrayed record)

You can find more details [here](https://choishingwan.gitlab.io/ukb-administration/pheno/understand_sql/)


## Program parameters
```
UK Biobank Phenotype SQL Constructor
0.2.6 ( 2021-03-02 )
https://gitlab.com/choishingwan/ukb_process/
(C) 2019-2021 Shing Wan (Sam) Choi
MIT License

This program will process the UK biobank Phenotype
information and generate a SQLite data base
===================================================
Usage: ukb_sql -d <Data showcase> -c <Code showcase> \
               -p <Phenotype> -o <Output>
Parameters:
    -d | --data     Data showcase file. Can be found here:
                    https://biobank.ndph.ox.ac.uk/~bbdatan/Data_Dictionary_Showcase.csv
    -c | --code     Data coding information. Can be found here:
                    https://biobank.ndph.ox.ac.uk/~bbdatan/Codings.csv
    -p | --pheno    UK Biobank decrypted Phenotype files
                    (R formatComma seperated list of phenotype
                    files can be provided
    -o | --out      Name of the generated database
    -g | --gp       gp_clinical table from ukbiobank
    -u | --drug     gp_scripts table from ukbiobank
    -w | --withdraw File contain ID of drop out samples
    -W | --drop     Update withdrawn status in existing database
    -D | --danger   Enable optioned that speed up processing
                    May generate corrupted database file if
                    server is unstable
    -m | --memory   Cache memory, default 1024byte
    -r | --replace  Replace existing ukb database file
    -v | --version  Get software version
    -h | --help     Display this help message
```
