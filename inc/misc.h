#ifndef MISC_H
#define MISC_H

#include "gzstream.h"
#include <algorithm>
#include <assert.h>
#include <chrono>
#include <ctime>
#include <filesystem>
#include <fstream>
#include <memory>
#include <sstream>
#include <string>
#include <string_view>
#include <unordered_map>
#include <unordered_set>
#include <vector>

namespace misc
{
inline bool remove_file(const std::string& in)
{
    std::filesystem::path f{in};
    return std::filesystem::remove(f);
}
inline bool file_exists(const std::string& file)
{
    std::filesystem::path f{file};
    return std::filesystem::exists(f);
}

inline bool is_gz_file(const std::string& name)
{
    const unsigned char gz_magic[2] = {0x1f, 0x8b};
    FILE* fp;
    if ((fp = fopen(name.c_str(), "rb")) == nullptr)
    { throw std::runtime_error("Error: Cannot open file - " + name); }
    unsigned char buf[2];
    if (fread(buf, 1, 2, fp) == 2)
    {
        if (buf[0] == gz_magic[0] && buf[1] == gz_magic[1]) { return true; }
        return false;
    }
    else
    {
        // can open the file, but can't read the magic number.
        return false;
    }
}

inline std::unique_ptr<std::istream>
load_stream(const std::string& filepath,
            std::ios_base::openmode mode = std::ios_base::in)
{
    if (filepath.empty())
        throw std::runtime_error("Error: Cannot open file with empty name");
    auto file = std::make_unique<std::ifstream>(filepath.c_str(), mode);
    if (!file->is_open())
    { throw std::runtime_error("Error: Cannot open file: " + filepath); }
    return std::unique_ptr<std::istream>(*file ? std::move(file) : nullptr);
}
inline std::unique_ptr<std::istream> load_stream(const std::string& filepath,
                                                 bool& gz_input)
{
    try
    {
        gz_input = misc::is_gz_file(filepath);
    }
    catch (const std::runtime_error& e)
    {
        throw std::runtime_error(e.what());
    }
    if (gz_input)
    {
        auto gz =
            std::make_unique<GZSTREAM_NAMESPACE::igzstream>(filepath.c_str());
        if (!gz->good())
        {
            throw std::runtime_error("Error: Cannot open file: " + filepath
                                     + " (gz) to read!\n");
        }
        return std::unique_ptr<std::istream>(*gz ? std::move(gz) : nullptr);
    }
    else
    {
        return load_stream(filepath);
    }
}

inline signed long long get_file_length(std::unique_ptr<std::istream>& file,
                                        bool is_gz)
{
    signed long long file_length = 0;
    if (!is_gz)
    {
        file->seekg(0, file->end);
        file_length = file->tellg();
    }
    file->clear();
    file->seekg(0, file->beg);
    return file_length;
}

inline void print_progress(std::unique_ptr<std::istream>& file,
                           signed long long length, double& prev_percentage,
                           bool is_gz)
{
    if (!is_gz)
    {
        double cur_loc = static_cast<double>(file->tellg());
        double cur_progress = (cur_loc / static_cast<double>(length)) * 100.0;
        if (cur_progress - prev_percentage > 0.01)
        {
            fprintf(stderr, "\rProcessing %03.2f%%", cur_progress);
            prev_percentage = cur_progress;
        }
        else if (prev_percentage >= 100.0)
        {
            fprintf(stderr, "\rProcessing %03.2f%%", 100.0);
        }
    }
}

inline void print_progress(std::size_t cur, std::size_t max,
                           double& prev_percentage)
{
    double cur_progress =
        (static_cast<double>(cur) / static_cast<double>(max)) * 100.0;
    if (cur_progress - prev_percentage > 0.01)
    {
        fprintf(stderr, "\rProcessing %03.2f%%", cur_progress);
        prev_percentage = cur_progress;
    }
    else if (prev_percentage >= 100.0)
    {
        fprintf(stderr, "\rProcessing %03.2f%%", 100.0);
    }
}

inline void ltrim(std::string& s)
{
    s.erase(s.begin(), std::find_if(s.begin(), s.end(),
                                    [](int ch) { return std::isgraph(ch); }));
}
inline void rtrim(std::string& s)
{
    s.erase(std::find_if(s.rbegin(), s.rend(),
                         [](int ch) { return std::isgraph(ch); })
                .base(),
            s.end());
}
inline void trim(std::string& s)
{
    ltrim(s);
    rtrim(s);
};

inline void split(const std::string& seq, std::vector<std::string>& res,
                  const std::string& separators = "\t ")
{
    res.clear();
    std::size_t prev = 0, pos;
    while ((pos = seq.find_first_of(separators, prev)) != std::string::npos)
    {
        if (pos > prev) res.emplace_back(seq.substr(prev, pos - prev));
        prev = pos + 1;
    }
    if (prev < seq.length()) res.emplace_back(seq.substr(prev, pos - prev));
}
template <class T>
inline void inline_split(T seq,
                         std::vector<T>* res,
                         const std::string& separators = "\t")
{
    std::size_t prev = 0, pos, idx = 0;
    while ((pos = seq.find_first_of(separators, prev)) != std::string::npos)
    {
        if (pos > prev) res->at(idx++) = (seq.substr(prev, pos - prev));
        prev = pos + 1;
    }
    if (prev < seq.length()) res->at(idx++) = (seq.substr(prev, pos - prev));
    if(idx > res->size()){
        res->resize(idx);
    }
}

inline bool str_end_with_csv_quote(std::string_view str,
                                   std::string::size_type start,
                                   std::string::size_type end)
{
    if (str.at(end - 1) != '"') return false;
    // This input is a single character of "
    if (start == end - 1) return true;
    // ukb escape sequence
    if (str.substr(end - 2, 2) == "\\\"") return false;
    if (str.substr(end - 2, 2) == "\"\"")
    {
        // normal escape, but need to check for \"" which can happen for ukb
        // wth \"", this is the end
        return str.substr(end - 3, 3) == "\\\"\"";
    }
    // this must be single \"
    return true;
}

inline void inplace_csv_split(const std::string& seq,
                              std::vector<std::string>& res)
{
    // logic is, for each item, we check if it starts with "
    // if it does starts with ", then we will keep appending to current until
    // we found an item that ends with ". This will fail if we have the
    // following csv entry "Slangit, the ,""Clean Slang"", Dictionary"
    std::size_t prev = 0, idx = 0, pos;
    bool is_quoted = false, is_end = false;
    while ((pos = seq.find_first_of(",", prev)) != std::string::npos)
    {

        if (pos > prev)
        {
            if (is_quoted) // continue of quote
            {

                is_end = str_end_with_csv_quote(seq, prev, pos);
                is_quoted = !is_end;
                res[idx - 1].append(","
                                    + seq.substr(prev, pos - prev - is_end));
            }
            else // first start
            {
                // empty input
                if (seq.substr(prev, pos - prev) == "\"\"")
                { res.at(idx++) = ""; } else
                {
                    // start with quote?
                    is_quoted = (seq.at(prev) == '"');
                    // end with quote, can only be an end if there are more than
                    // one character
                    is_end = str_end_with_csv_quote(seq, prev, pos)
                             && (pos - prev > 1);
                    // skip the "
                    res.at(idx++) = seq.substr(prev + is_quoted,
                                               pos - prev - is_quoted - is_end);
                    is_quoted &= !is_end;
                }
            }
        }
        else
        {
            // empty comma

            if (is_quoted) { res[idx - 1].append(","); }
            else
            {
                res.at(idx++) = "";
            }
        }
        prev = pos + 1;
    }
    if (prev < seq.length())
    {
        if (is_quoted)
        {
            // only do remove quote if last item really is a quote
            res[idx - 1].append(
                ","
                + seq.substr(prev, seq.length() - prev - (seq.back() == '"')));
        }
        else
        {
            is_quoted = seq.at(prev) == '"';
            is_end = seq.back() == '"';
            res.at(idx++) = seq.substr(
                prev + is_quoted, seq.length() - prev - is_end - is_quoted);
        }
    }
    else
    {
        res.at(idx++) = "";
    }
}

inline void csv_split(const std::string& seq, std::vector<std::string>& res)
{
    // logic is, for each item, we check if it starts with "
    // if it does starts with ", then we will keep appending to current until
    // we found an item that ends with ". This will fail if we have the
    // following csv entry "Slangit, the ,""Clean Slang"", Dictionary"
    std::size_t prev = 0, pos;
    res.clear();
    bool is_quoted = false, is_end = false;
    while ((pos = seq.find_first_of(",", prev)) != std::string::npos)
    {

        if (pos > prev)
        {
            if (is_quoted) // continue of quote
            {
                is_end = str_end_with_csv_quote(seq, prev, pos);
                is_quoted = !is_end;
                res.back().append("," + seq.substr(prev, pos - prev - is_end));
            }
            else // first start
            {
                // empty input
                if (seq.substr(prev, pos - prev) == "\"\"")
                { res.emplace_back(""); } else
                {
                    // start with quote?
                    is_quoted = (seq.at(prev) == '"');
                    // end with quote, can only be an end if there are more than
                    // one character
                    is_end = str_end_with_csv_quote(seq, prev, pos)
                             && (pos - prev > 1);
                    // skip the "
                    res.emplace_back(seq.substr(
                        prev + is_quoted, pos - prev - is_quoted - is_end));
                    is_quoted &= !is_end;
                }
            }
        }
        else
        {
            // empty comma
            if (is_quoted)
                res.back().append(",");
            else
                res.emplace_back("");
        }
        prev = pos + 1;
    }
    if (prev < seq.length())
    {
        if (is_quoted)
        {
            // only do remove quote if last item really is a quote
            res.back().append(
                ","
                + seq.substr(prev, seq.length() - prev - (seq.back() == '"')));
        }
        else
        {
            is_quoted = seq.at(prev) == '"';
            is_end = seq.back() == '"';
            res.emplace_back(seq.substr(
                prev + is_quoted, seq.length() - prev - is_end - is_quoted));
        }
    }
    else
    {
        res.emplace_back("");
    }
}

// NOTE: Didn't work for non-ASCII characters
inline void to_upper(std::string& str)
{
    std::transform(str.begin(), str.end(), str.begin(), ::toupper);
}
inline void remove_quote(std::string& str)
{
    str.erase(std::remove(str.begin(), str.end(), '\"'), str.end());
}
inline void remove_surround_quote(std::string& str)
{
    const auto start = (*str.begin() == '"');
    const auto end = (str.back() == '"');
    str = str.substr(0 + start, str.length() - end - start);
}

class Convertor
{
public:
    template <typename T>
    static T convert(const std::string& str)
    {
        errno = 0;
        std::istringstream iss(str);
        T obj;
        iss >> obj;
        if (!iss.eof() || iss.fail())
        { throw std::runtime_error("Unable to convert the input"); }
        if constexpr (std::is_same_v<T, size_t>)
        {
            // easiest way to check for negative
            if (str.at(0) == '-')
            {
                throw std::runtime_error("Error: Negative input for a positive "
                                         "variable");
            }
        }
        return obj;
    }

private:
    static std::istringstream iss;
};
// wrapper for to make things easier
template <typename T>
inline T convert(const std::string& str)
{
    return Convertor::convert<T>(str);
}

struct string_hash
{
    using is_transparent = void;
    [[nodiscard]] size_t operator()(const char* txt) const
    {
        return std::hash<std::string_view>{}(txt);
    }
    [[nodiscard]] size_t operator()(std::string_view txt) const
    {
        return std::hash<std::string_view>{}(txt);
    }
    [[nodiscard]] size_t operator()(const std::string& txt) const
    {
        return std::hash<std::string>{}(txt);
    }
};

inline void remove_quote(const char** start, uint32_t* slen)
{
    assert((*start) != nullptr);
    if (*slen == 0) return;
    if ((*start)[(*slen) - 1] == '"') { --(*slen); }
    // now check if we have an escaped quote at the end
    if ((*start)[(*slen) - 1] == '\\') { --(*slen); }
    if ((*start)[0] == '"')
    {
        ++(*start);
        --(*slen);
    }
}
inline const std::string get_today_date()
{
    std::chrono::system_clock::time_point now =
        std::chrono::system_clock::now();
    std::time_t now_c = std::chrono::system_clock::to_time_t(now);
    std::tm now_tm = *std::localtime(&now_c);
    const std::string today = std::to_string(now_tm.tm_year + 1900) + "-"
                              + std::to_string(now_tm.tm_mon) + "-"
                              + std::to_string(now_tm.tm_mday);
    return today;
}
template <typename T>
using unordered_str_key_map =
    std::unordered_map<std::string, T, misc::string_hash, std::equal_to<>>;
using unordered_str_key_set =
    std::unordered_set<std::string, misc::string_hash, std::equal_to<>>;

} // namespace misc
#endif // MISC_H
