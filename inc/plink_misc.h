#ifndef PLINK_MISC
#define PLINK_MISC
#include <assert.h>
#include <misc.h>
#include <plink2_string.h>
#include <plink2_text.h>
namespace plink_misc
{

inline void next_token(const char** token_start, const char** token_end,
                       const char* line_end, const char delim = '\t')
{
    assert(token_start != nullptr);
    // we will get the end of token
    *token_end = plink2::AdvToDelimOrEnd(*token_start, line_end, delim);
    // now we need to check if the , is within a "" block, if true, then we
    // should
    bool has_quote = false, escape = false;
    bool is_escaped_quote = false;
    const char* token_iter = *token_start;
    do
    {
        for (; token_iter < *token_end && token_iter < line_end; ++token_iter)
        {
            if (token_iter[0] == '\\') { escape = true; }
            else if (token_iter[0] == '\"')
            {
                if (!escape) { has_quote = !has_quote; }
                else
                {
                    is_escaped_quote = true;
                }
                escape = false;
            }
            else
            {
                is_escaped_quote = false;
                escape = false;
            }
        }
        // in theory, token_iter should now be pointing to token_end

        if (has_quote && *token_end == line_end)
        {
            throw std::runtime_error("Error: Malformed csv format?");
        }
        else if (has_quote)
        {
            if (is_escaped_quote)
            {
                // this seems like a mis-specified input, for now, just ignore
                // and return
                return;
            }
            else
            {
                ++(*token_end);
                *token_end =
                    plink2::AdvToDelimOrEnd(*token_end, line_end, delim);
            }
        }
    } while (has_quote);
}
inline void next_csv_token(const char** token_start, const char** token_end,
                           const char* line_end)
{
    next_token(token_start, token_end, line_end, ',');
}

inline size_t get_field_id_info(std::string_view in,
                                std::vector<std::string_view>* res, bool* is_rg)
{
    if (is_rg)
    {
        if (res->size() > 3) res->resize(3);
        const char* start = in.data();
        const char* token_end = in.data() + in.length();
        const char* end = start;
        for (; end < token_end; ++end)
        {
            if (end[0] == '-') { break; }
        }
        // next_token(&start, &end, token_end, '-');
        if (end >= token_end)
        { throw std::runtime_error("End bigger than already"); }
        res->at(0) = std::string_view(start, end - start);
        while (end[0] == '-') { ++end; }
        start = end;
        for (; end < token_end; ++end)
        {
            if (end[0] == '.') { break; }
        }
        if (end >= token_end) { throw std::runtime_error("what?"); }
        res->at(1) = std::string_view(start, end - start);
        start = ++end;
        for (; end < token_end; ++end)
        {
            if (end[0] == '.') { break; }
        }
        if (end != token_end) { throw std::runtime_error("Not equal to end"); }
        res->at(2) = std::string_view(start, end - start);
        start = ++end;
    }
    else
    {
        // normal stuff
        misc::inline_split(in, res, ".");
        if (res->size() < 4)
        {
            // this is likely rg, give it a try
            *is_rg = true;
            return get_field_id_info(in, res, is_rg);
        }
    }
}

} // namespace plink_miscag
#endif