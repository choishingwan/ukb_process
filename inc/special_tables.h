#ifndef SPECIAL_TABLES_H
#define SPECIAL_TABLES_H
#include "sql_table.h"
#include <sqlite3.h>
#include <vector>
#include "misc.h"
#include <unordered_set>

namespace special_tables
{

inline void load_provider(sqlite3* db)
{
    SQL_table gp_provider("gp_provider", db);
    gp_provider.create_table_and_statement(std::vector<sqlColumn> {
        sqlColumn("gp_id").INT().primary_key().not_null(),
        sqlColumn("gp_name").TEXT().not_null()});
    gp_provider.execute_sql("insert into gp_provider (gp_id, gp_name) "
                            "VALUES(1, \"England(Vision)\")");
    gp_provider.execute_sql("insert into gp_provider (gp_id, gp_name) "
                            "VALUES(2, \"Scotland\")");
    gp_provider.execute_sql("insert into gp_provider (gp_id, gp_name) "
                            "VALUES(3, \"England(TPP)\")");
    gp_provider.execute_sql("insert into gp_provider (gp_id, gp_name) "
                            "VALUES(4, \"Wales\")");
    gp_provider.create_index(std::vector<std::string> {"gp_id"},
                             "PROVIDER_INDEX");
}

inline std::string transform_date(const std::string& date, std::vector<std::string>& token)
{
    misc::split(date,token,  "/");
    if (token.size() != 3)
    { throw std::runtime_error("Error: Undefined date format: " + date); }
    return std::string(token[2] + "-" + token[1] + "-" + token[0]);
}
void load_primary_care_tables(const misc::unordered_str_key_set &fields_in_db, const std::string & gp, const std::string& drug, sqlite3 *db);
}


#endif // SPECIAL_TABLES_H
