#include "commander.h"

Commander::Commander() {}

int Commander::parse_command(int argc, char *argv[]) {
  if (argc < 2) {
    usage();
    return -1;
  }
  static const char *optString = "s:d:c:p:o:m:g:u:w:raWDh?v";
  static const struct option longOpts[] = {
      {"data", required_argument, nullptr, 'd'},
      {"code", required_argument, nullptr, 'c'},
      {"csv", no_argument, nullptr, 's'},
      {"pheno", required_argument, nullptr, 'p'},
      {"out", required_argument, nullptr, 'o'},
      {"memory", required_argument, nullptr, 'm'},
      {"gp", required_argument, nullptr, 'g'},
      {"add", no_argument, nullptr, 'a'},
      {"drug", required_argument, nullptr, 'u'},
      {"replace", no_argument, nullptr, 'r'},
      {"danger", no_argument, nullptr, 'D'},
      {"withdrawn", required_argument, nullptr, 'w'},
      {"threads", required_argument, nullptr, 't'},
      {"drop", no_argument, nullptr, 'W'},
      {"version", no_argument, nullptr, 'v'},
      {"help", no_argument, nullptr, 'h'},
      {nullptr, 0, nullptr, 0}};
  int longIndex = 0;
  int opt = 0;
  opt = getopt_long(argc, argv, optString, longOpts, &longIndex);

  while (opt != -1) {
    switch (opt) {
    case 'd':
      m_data_showcase = optarg;
      break;
    case 'm':
      m_memory = optarg;
      break;
    case 'D':
      m_danger = true;
      break;
    case 'c':
      m_code_showcase = optarg;
      break;
    case 'p':
      m_pheno_name = optarg;
      break;
    case 'o':
      m_out_name = optarg;
      break;
    case 'r':
      m_replace = true;
      break;
    case 'a':
      m_addition = true;
      break;
    case 'g':
      m_gp_name = optarg;
      break;
    case 't':
      m_threads = misc::convert<std::size_t>(optarg);
      break;
    case 'u':
      m_drug_name = optarg;
      break;
    case 'w':
      m_drop_name = optarg;
      break;
    case 'W':
      m_update_withdrawn = true;
      break;
    case 's':
      m_is_csv = true;
      break;
    case 'v':
      std::cerr << "ukb_sql: " << version << std::endl;
      return 0;
    case 'h':
    case '?':
      usage();
      return 0;
    default:
      throw "Undefined operator, please use --help for more "
            "information!";
    }
    opt = getopt_long(argc, argv, optString, longOpts, &longIndex);
  }
  if (!valid_inputs()) {
    return -1;
  }
  return 1;
}

bool Commander::valid_inputs() {
  bool error = false;
  std::string error_msg;
  if (!m_update_withdrawn) {
    // Has phenotype input
    if (!m_pheno_name.empty()) {
      if (m_data_showcase.empty()) {
        error = true;
        error_msg.append(
            "Error: You must provide the data showcase csv file!\n");
      }
      if (m_code_showcase.empty()) {
        error = true;
        error_msg.append(
            "Error: You must provide the code showcase csv file!\n");
      }
    } else if (m_pheno_name.empty() && m_gp_name.empty() &&
               m_drug_name.empty()) {
      // none-provided
      error = true;
      error_msg.append(
          "Error: You must provide either the phenotype, GP records!\n");
    }
  } else {
    if (m_drop_name.empty()) {
      error = true;
      error_msg.append("Error: You must provide the withdrawn sample "
                       "file if you wish to update the withdrawn information");
    }
  }
  if (error) {
    std::cerr << error_msg << std::endl
              << "Please check you have all the required input!" << std::endl;
  }
  return !error;
}

void Commander::usage() {
  std::string usage_str =
      "UK Biobank Phenotype SQL Constructor\n" + version + " ( " + date +
      " )\n" + "https://gitlab.com/choishingwan/ukb_process/\n" +
      "(C) 2019-2022 Shing Wan (Sam) Choi\n" +
      "MIT License\n\n"
      "This program will process the UK biobank Phenotype\n"
      "information and generate a SQLite data base\n\n"
      "===================================================\n"
      "Usage: ukb_sql -d <Data showcase> -c <Code showcase> \\\n"
      "               -p <Phenotype> -o <Output> \n"
      "Parameters:\n"
      "    -d | --data     Data showcase file. Can be found here:\n"
      "                    "
      "https://biobank.ndph.ox.ac.uk/~bbdatan/"
      "Data_Dictionary_Showcase.csv\n"
      "    -c | --code     Data coding information. Can be found here:\n"
      "                    "
      "https://biobank.ndph.ox.ac.uk/~bbdatan/Codings.csv\n"
      "    -p | --pheno    UK Biobank decrypted Phenotype files\n"
      "                    (R format tab seperated list of phenotype \n"
      "                    files can be provided)\n"
      "    -s | --csv      Indicate that all phenotype files are csv\n"
      "    -o | --out      Name of the generated database\n"
      "    -g | --gp       gp_clinical table from ukbiobank\n"
      "    -u | --drug     gp_scripts table from ukbiobank\n"
      "    -w | --withdraw File contain ID of drop out samples\n"
      "    -W | --drop     Update withdrawn status in existing database\n"
      "    -D | --danger   Enable optioned that speed up processing\n"
      "                    May generate corrupted database file if\n"
      "                    server is unstable\n"
      "    -m | --memory   Cache memory, default 1024byte\n"
      "    -r | --replace  Replace existing ukb database file\n"
      "    -v | --version  Get software version\n"
      "    -h | --help     Display this help message\n\n";
  std::cerr << usage_str << std::endl;
}
