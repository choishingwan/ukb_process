#include "sql_table.h"
SQL_table&
SQL_table::create_table_and_statement(const std::vector<sqlColumn>& columns,
                                      const bool verbose)
{
    std::string table_creation = "CREATE TABLE " + m_table_name + "(";
    std::string statement_preparation = "INSERT INTO " + m_table_name + "(";
    std::string end_statement = "VALUES (";
    std::vector<std::string> foreign_keys;
    for (auto&& col : columns)
    {
        // add item to table creation statement
        table_creation.append(col.get_column_statement());
        // add item to statement
        statement_preparation.append(col.get_name());
        // add item to end of the sql statement
        end_statement.append("@" + col.get_name());
        if (col.has_foreign()) foreign_keys.push_back(col.get_foreign());
        if (&col != &columns.back())
        {

            // not the last item, so we need the ,
            table_creation.append(",");
            statement_preparation.append(",");
            end_statement.append(",");
        }
    }
    // we have processed all columns, can end the statements
    statement_preparation.append(")");
    end_statement.append(")");
    // now add the foreign keys to the table creation statement
    for (auto&& foreign : foreign_keys)
    { table_creation.append("," + foreign); }
    table_creation.append(")");
    create_table(table_creation, verbose);
    prep_statement(statement_preparation + end_statement);
    return *this;
}


void SQL_table::create_table(const std::string& sql, const bool verbose)
{
    try
    {
        execute_sql(sql, true);
        if (verbose)
            std::cerr << "Table: " << m_table_name << " created sucessfully\n";
        m_table_constructed = true;
    }
    catch (const std::runtime_error& er)
    {
        std::cerr << "Failed to create: " << m_table_name << "\n";
        throw std::runtime_error(er.what());
    }
}

void SQL_table::prep_statement(const std::string& sql)
{
    if (!m_table_constructed)
        throw std::runtime_error("Error: Table: " + m_table_name
                                 + " not created");
    sqlite3_prepare_v2(m_db, sql.c_str(), -1, &m_statement, nullptr);
}
